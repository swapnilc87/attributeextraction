package util;

import java.util.HashMap;

public class CountMap<T> extends HashMap<T,Integer> {
    final Double ALPHA = 0.00001;
    double total;

    public void add(T key) {
        this.put(key,this.getOrDefault(key,0)+1);
    }

    public Integer count(T key) {
        return this.getOrDefault(key,0);
    }

    public void calculateTotal() {
        total = this.values().stream().mapToInt(Integer::intValue).sum()/*+this.keySet().size()*ALPHA*/;
    }

    public Double probability(T key) {
        double current = count(key)+ALPHA; //smoothing

        return current/total;
    }

}
