import org.apache.commons.lang3.StringUtils;
import util.CountMap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

public class WordCount {
    public static String bigram(String a, String b) {
        return String.join(" ", a, b);
    }

    public static String[] splitBigram(String input) {
        return input.split(" ");
    }

    public static boolean isAlphabetic(String input) {
        if (input.matches("[A-Za-z\\- ]+")) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        List<String> lines = readFile("/Users/300032675/searchWorkspace/data/solr_products/lower_titles.txt");

        System.out.println("finished reading file:" + lines.size());
        CountMap<String> unigrams = new CountMap<>();
        CountMap<String> bigrams = new CountMap<>();
        Set<String> candidates = new HashSet<>();
        CountMap<String> firstWordMap = new CountMap<>();
        CountMap<String> secondWordMap = new CountMap<>();

        for (String line : lines) {
            String words[] = line.split("\\s+");
            if (words.length > 1) {
                int start = Math.max(0, words.length - 2);
                unigrams.add(words[0]);
                for (int i = 1; i < words.length; i++) {
                    String previousWord = words[i - 1];
                    String currentWord = words[i];

                    firstWordMap.add(previousWord);
                    secondWordMap.add(currentWord);
                    if (isAlphabetic(previousWord) && isAlphabetic(currentWord)) {
                        bigrams.add(bigram(previousWord, currentWord));
                        if (i > start) {
                            candidates.add(bigram(previousWord, currentWord));
                        }
                    }
                    unigrams.add(currentWord);
                }
            }
        }
        bigrams.calculateTotal();
        unigrams.calculateTotal();
        firstWordMap.calculateTotal();
        secondWordMap.calculateTotal();
        Map<String, Double> candidateBigrams = new TreeMap<>();

        System.out.println("finished computing ngrams");
        for (String bigram : candidates) {
            if (bigrams.count(bigram) < 20) {
                continue;
            }
            String[] words = splitBigram(bigram);
            double numerator = Math.log10(firstWordMap.probability(words[0]) * secondWordMap.probability(words[1]));
            double denominator = Math.log10(bigrams.probability(bigram));
            double npmi = (numerator / denominator) - 1;
            if (npmi > 0.4) {
                candidateBigrams.put(bigram, npmi);
            }
        }

//        System.out.println(candidateBigrams);
        System.out.println();
//        System.out.println(bigrams);
        for (String candidate : candidateBigrams.keySet()) {
//            if(candidate.contains("saree")) {
            System.out.println(candidate + " : " + candidateBigrams.get(candidate) + " : " + bigrams.get(candidate));
//            }
        }
    }

    public static List<String> readFile(String fileName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName));
        ) {
            return reader.lines().collect(Collectors.toList());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
